import os
import subprocess


exports = [
    '-e png_256/%s.png -w 256',
]


for svg in os.listdir('svg'):
    basename, ext = os.path.splitext(svg)
    if ext != '.svg':
        continue

    for export in exports:
        export %= basename
        cmd = 'inkscape -f svg/%s %s' % (svg, export)
        print(cmd)
        subprocess.call(cmd, shell=True)
