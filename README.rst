===========================
Icons for the energy domain
===========================

Grid components
===============

.. image:: png_256/bus.png
   :width: 64px
   :alt: Bus / Node

.. image:: png_256/grid.png
   :width: 64px
   :alt: Grid

.. image:: png_256/pylon.png
   :width: 64px
   :alt: Pylon

.. image:: png_256/transformer.png
   :width: 64px
   :alt: Transformer (vertical)

.. image:: png_256/transformer-horizontal.png
   :width: 64px
   :alt: Transformer (horizontal)


Monitoring and control
======================

.. image:: png_256/agent.png
   :width: 64px
   :alt: Agent

.. image:: png_256/control_strategy.png
   :width: 64px
   :alt: Control strategy


Conventional generation
=======================

.. image:: png_256/gas_turbine_power_plant.png
   :width: 64px
   :alt: Gas turbine power plant

.. image:: png_256/large_power_plant.png
   :width: 64px
   :alt: Large power plant

.. image:: png_256/medium_power_plant.png
   :width: 64px
   :alt: Medium power plant


Regenerative generation
=======================

.. image:: png_256/pv.png
   :width: 64px
   :alt: Photo voltaic

.. image:: png_256/chp.png
   :width: 64px
   :alt: Chp

.. image:: png_256/wecs.png
   :width: 64px
   :alt: Wind energy converter system

Converter
=======================

.. image:: png_256/air_source_heat_pump.png
   :width: 64px
   :alt: Air source heat pump

Consumers
=========

.. image:: png_256/household.png
   :width: 64px
   :alt: Household


Storage
=======

.. image:: png_256/battery.png
   :width: 64px
   :alt: Battery storage

.. image:: png_256/ev.png
   :width: 64px
   :alt: Electric vehicle


Misc.
=====

.. image:: png_256/market.png
   :width: 64px
   :alt: Market

.. image:: png_256/profile.png
   :width: 64px
   :alt: Load or generation profile
